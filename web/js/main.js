// hide both spans on launch
$("span").hide();


// Image Button
// pull the image from the array with a random number
// set the src to the image from the array
// hide gif span on click
// show image span on click
$("#imagebtn").on('click', function () {
    var i = images[Math.floor(Math.random()*images.length)];
    $("#image").attr("src",i);
    $("#gifspan").hide();
    $("#imagespan").show();
});

// Gif Button
// pull the gif from the array with a random number
// set the src to the gif from the array
// hide image span on click
// show gif span on click
$("#gifbtn").on('click', function () {
    var i = gifs[Math.floor(Math.random()*gifs.length)];
    $("#gif").attr("src",i);
    $("#imagespan").hide();
    $("#gifspan").show();
});

// Image Array
var images = ["https://consequenceofsound.files.wordpress.com/2015/03/idris-elba.png", "https://www.filmibeat.com/img/2017/03/idris-elba-wants-to-utilise-his-professional-k1-kickboxer-training-in-films-28-1490704114.jpg", "https://photos.laineygossip.com/articles/idris-elba-costume-05jul16-18.jpg", "https://i.dailymail.co.uk/i/pix/2014/08/14/1407971905322_wps_1_http_www_details_com_cele.jpg", "https://www.celebritysizes.com/wp-content/uploads/2017/06/Idris-Elba.jpg"];

// Gif Array
var gifs = ["https://media.giphy.com/media/QvKnMnW9a3gXe/giphy.gif", "https://media.giphy.com/media/kgn9UdY7IgnIs/giphy.gif", "https://media.giphy.com/media/7KH1uzOTwWeek/giphy.gif", "https://media.giphy.com/media/spHUq6KbcA3G8/giphy.gif", "https://media.giphy.com/media/KTgZvodtcfnQA/giphy.gif"];